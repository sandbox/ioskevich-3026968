<?php

/**
 * @file
 * Integration with the Rules module.
 */

/**
 * Implements hook_rules_action_info().
 */
function pantheon_cache_expire_rules_action_info() {
  return array(
    'pantheon_cache_expire_rules_action_flush_url' => array(
      'label' => t('Clear URL(s) from the page and Global CDN cache.'),
      'group' => 'Pantheon',
      'parameter' => array(
        'urls' => array(
          'type' => 'text',
          'label' => t('Relative paths of page to clear'),
          'description' => t('Enter one value in each line. Examples: node, node/1, taxonomy/term/10, user/7. If some path is aliased, make sure to add alias as well.'),
        ),
      ),
      'callbacks' => array(
        'validate' => 'pantheon_cache_expire_rules_action_flush_url_validation',
      ),
    ),
  );
}

/**
 * Expires pages from cache_page bin and Pantheon's edge.
 *
 * @param $urls
 *   Array with user-defined URLs and internal paths.
 */
function pantheon_cache_expire_rules_action_flush_url($urls) {
  global $base_url;

  $paths = array();
  $urls = explode("\r\n", $urls);
  foreach ($urls as &$url) {
    if (!empty($url)) {
      $url = trim($url);
      if (strpos($url, $base_url) === 0) {
        $paths[] = str_replace($base_url . '/', '', $url);
      }
      else {
        $paths[] = $url;
      }
    }
  }
  
  if (!empty($paths)) {
    // Flush Drupal's cache_page bin first.
    foreach ($paths as $path) {
      cache_clear_all($path, 'cache_page');
    }
    // Attempt to flush Pantheon's edge cache (GlobalCDN) by 
    // calling a function exposed in /srv/includes/pantheon.php on Pantheon codeserver.
    if (function_exists('pantheon_clear_edge_paths')) {
      try {
        pantheon_clear_edge_paths($paths);
      } catch (Exception $e) {
        watchdog('pantheon_cache_expire', 'Pantheon API returned an exception: @exception', array('@exception' => $e->getMessage()), WATCHDOG_ERROR);
      }
    }
  }
}

/**
 * Process value submitted by user in Rules settings.
 */
function pantheon_cache_expire_rules_action_flush_url_validation($action) {

  // If user uses PHP values in his input, some line ending might be lost.
  // To avoid this bug we should add a whitespace to the end of each value.
  $urls = &$action->settings['urls'];
  $url_parts = explode("\r\n", $urls);
  foreach ($url_parts as &$url) {
    $url = trim($url) . ' ';
  }
  $urls = implode("\r\n", $url_parts);
}
